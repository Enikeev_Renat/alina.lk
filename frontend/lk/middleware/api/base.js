import axios from './instance'

export default {
	commonInfo() {
		return axios.post('', {action: 'get_common_info'}).then(r => r.data)
	},

	getMenu() {
		return axios.post('', {action: 'get_menu'}).then(r => r.data)
	},

}
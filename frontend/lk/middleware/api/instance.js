import axios from 'axios'

axios.defaults.baseURL = apiUrl;

axios.defaults.headers.post['Content-Type'] = 'multipart/form-data';

axios.defaults.transformRequest = [function (data) {

	let formData = new FormData();

	Object.keys(data).map(function(key) {
		if (Array.isArray(data[key])) {

			for (let i = 0; i < data[key].length; i++) {
				formData.append(`${key}[]`, typeof data[key][i] === 'object' ?  JSON.stringify(data[key][i]) : data[key][i]);
			}

		} else {
			formData.append(key, data[key]);
		}
	});

	return formData;
}];



/*
axios.interceptors.request.use(config => {
	return config;
}, error => {
	return Promise.reject(error);
});*/

axios.interceptors.response.use(
	response => {
		if ( isProd && response.data && response.data.user && response.data.user.not_authoruze ) location.href = 'https://alinamakarova.ru/';
		return response;
	},
	error => {
		return Promise.reject(error);
	}
);



export default axios;

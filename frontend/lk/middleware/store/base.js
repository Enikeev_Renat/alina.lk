import {base as Api} from '../api'

import navigation from '../../components/layout/navigation';

let navCountBuffer = {
	courses: {},
	dialogs: {},
};

export default {
	namespaced: true,

	state: {

		websocket: null,

		showMenu: false,

		breakpoint: {
			mobile: false,
			tablet: false,
			compact: false,
			desktop: false,
			hd: false,
			current: ''
		},

		activeNav: null,

		user: {
			id: null,
			photo: '',
			name: '',
			email: '',
			curator: false,
			register_date: '',
		},

		sections: {
			dialogs: true,
			courses: true,
		},

		navigation,

		localNav: {
			courses: null,
			dialogs: null,
		},

		gallery: {
			items: [],
			open: null
		}
	},

	mutations: {
		TOGGLE_MENU(state, value){
			state.showMenu = value !== undefined ? !!value : !state.showMenu;
		},

		SET_WEBSOCKET(state, value){
			state.websocket = value;
		},

		SET_GALLERY(state, {gallery, open}){
			state.gallery.items = gallery;
			state.gallery.open = open;
		},

		SET_GALLERY_OPEN(state, open){
			state.gallery.open = open;
		},

		SET_COMMON_INFO(state, {user, sections}){
			state.user = user;
			state.sections = Object.assign({}, state.sections, sections || {});
		},

		SET_ACTIVE_NAV(state, id){
			state.activeNav = id || null;
		},

		SET_MENU(state, value){

			let nav = [...navigation];

			Object.keys(value).forEach( name => {
				let idx = nav.findIndex( i => i.name === name );

				if ( idx >= 0 && nav[idx] ){
					let count = 0;
					const submenu = value[name].map( item => {
						if (item.count) count += item.count;
						return {
							key_id: item.id,
							title: item.title,
							count: item.count,
							route: `${nav[idx].route}/${item.id}`,
						}
					});

					nav[idx].submenu = [...nav[idx].submenu, ...submenu];

					nav[idx].count = count;
				}
			});

		},

		SET_VIEWPORT(state, bp){
			Object.keys(state.breakpoint).map((key)=>{
				state.breakpoint[key] = key === bp;
			});
			state.breakpoint.current = bp;
		},

		SET_LOCAL_NAV(state, value){
			if ( value && value.menu ){
				state.localNav[value.section] = value.menu.map(item => {
					if ( !item.count && navCountBuffer[value.section][item.id.toString()] ) {
						item.count = navCountBuffer[value.section][item.id];
					}
					navCountBuffer[value.section] = {};
					return item;
				});
			}
		},

		SET_LOCAL_NAV_COUNT(state, {name, id, count}){
			if ( state.localNav[name] && state.localNav[name].length ){
				const idx = state.localNav[name].findIndex( item => item.id.toString() === id.toString());
				if (idx >= 0) state.localNav[name][idx].count = count;
			} else {
				navCountBuffer[name][id.toString()] = count;
			}
		}

	},

	actions: {

		getMenu(context){
			return Api.getMenu()
				.then((res)=>{
					context.commit('SET_MENU', res);
				})
			;
		},

		commonInfo(context){
			return Api.commonInfo()
				.then((res)=>{
					context.commit('SET_COMMON_INFO', res);
				})
			;
		},
	},

	getters: {

		navigation: state =>{
			let collect = {},
				menu = []
			;

			state.navigation
				.filter( item => {
					return state.sections[item.name] !== false;
				})
				.forEach( item => {

				if ( collect[item.group] === undefined ) {
					collect[item.group] = menu.length;
					menu.push({
						title: item.group,
						sections: []
					});
				}

				menu[collect[item.group] ].sections.push(item);

			});

			return menu;
		},


	}
}
import Vue from 'vue'
import Router from 'vue-router'
import routes from './routes'

Vue.use(Router);

const router = new Router({
	base: isLocal || isDev ? '/' : '/lk/',
	mode: isLocal || isDev ? 'hash' : 'history',
	routes,
	linkActiveClass: 'is-active',
	linkExactActiveClass: 'is-active',
	transitionOnLoad: true,
	root: '/'
});

router.afterEach((to, from) => {
	document.title = to.meta.title;
	window.scrollTo(0, 0);
});

export default router
import Error from '../../views/error'

export default [
	{
		path: '/',
		redirect: '/dialogs',
		name: 'home',
		components: import('../../views/development'),
		meta: {title: 'Раздел в разработке'}
	},
	{
		path: '/courses',
		component: ()=> import('../../views/courses'),
		name: 'courses',
		meta: {
			title: 'Мои курсы',
			section: 'courses'
		}
	},
	{
		path: '/courses/all',
		component: ()=> import('../../views/courses'),
		name: 'courses-all',
		meta: {
			title: 'Мои курсы',
			section: 'courses'
		}
	},
	{
		path: '/courses/:case',
		component: ()=> import('../../views/courses/item'),
		name: 'courses-case',
		meta: {
			title: 'Мои курсы',
			section: 'courses'
		}
	},
	{
		path: '/courses/:case/:item',
		component: ()=> import('../../views/courses/item'),
		name: 'courses-item',
		meta: {
			title: 'Мои курсы',
			section: 'courses'
		}
	},

	{
		path: '/dialogs',
		// redirect: '/dialogs/all',
		component: ()=> import('../../views/dialogs'),
		name: 'dialogs',
		meta: {
			title: 'Сообщения',
			section: 'dialogs'
		}
	},

	{
		path: '/dialogs/:case',
		component: ()=> import('../../views/dialogs'),
		name: 'dialogs-case',
		meta: {
			title: 'Сообщения',
			section: 'dialogs'
		}
	},

	{
		path: '/dialogs/:case/:item',
		component: ()=> import('../../views/dialogs'),
		name: 'dialogs-item',
		meta: {
			title: 'Сообщения',
			section: 'dialogs'
		}
	},

	{path: '/notifications',name: 'notifications',components: import('../../views/development'),meta: {title: 'Раздел в разработке'}},
	{path: '/cart',name: 'cart',components: import('../../views/development'),meta: {title: 'Раздел в разработке'}},
	{path: '/lessons',name: 'lessons',components: import('../../views/development'),meta: {title: 'Раздел в разработке'}},
	{path: '/support',name: 'support',components: import('../../views/development'),meta: {title: 'Раздел в разработке'}},
	{path: '/faq',name: 'faq',components: import('../../views/development'),meta: {title: 'Раздел в разработке'}},

	{
		path: '*/*',
		component: ()=> import('../../views/error'),
		name: '404',
		meta: {
			title: 'Страница не существует'
		}
	}
];


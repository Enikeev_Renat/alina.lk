<?php
header('Access-Control-Allow-Origin: *');
sleep(1.5);

$data = array();

$action = $_POST['action']; // метод запроса {{string}}

$is_curator = true;

switch ($action) {

	case 'get_common_info':

		$data['user'] = array(
			"id" => 555,
			"photo" => "https://alinamakarova.ru/bitrix/templates/alina_super_new/img/picture/photo_1.jpg",
			"name" => "Евгения Удельная",
			"email" => "web.eliseeva@gmail.com",
			"register_date" => "21.01.2017",
			"curator" => $is_curator, //
		);
		break;


	case 'get_nav_courses':

		$data['menu'] = array(
			array(
				"id" => 51,
				"title" => "Муссовые торты",
				"count" => 0,
			),
			array(
				"id" => 152,
				"title" => "Современная выпечка",
				"count" => 2,
			),
		);

		break;

	case 'get_nav_dialogs':

		$data['menu'] = array(
			array(
				"id" => 51,
				"title" => "Мегакурс 1",
				"count" => 0,
			),
			array(
				"id" => 152,
				"title" => "Кондитер 2.0",
				"count" => 0,
			),
			array(
				"id" => 84,
				"title" => "Современная выпечка",
				"count" => 150,
			),
			array(
				"id" => 61,
				"title" => "Важные",
				"count" => 87,
			),
		);

		break;

	case 'get_course_all':

		$data['items'] = array(
			array(
				"image" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/db37ccd8615a28dfeab5834f042a7264.jpg",
				"title" => "Муссовые торты",
				"text" => "Новый Кондитерский Курс – революция в сфере кондитерского образования. Теория, рецепты, декор, бизнес: всё, что нужно для успешного старта сладкого бизнеса в одном месте.",
				"id" => 51,
				"status" => array(
					"text" => "Не оплачен",
					"type" => "error",  // success, error, warning
				),
				"btns" => array(
					array(
						array(
							"url" => "/",
							"yellow" => true,
							"text" => "Купить 17 900 ₽",
						),
						array(
							"url" => "/",
							"yellow" => true,
							"text" => "В рассрочку 8 950 ₽ / мес",
						),
					),
					array(
						"course_id" => 51,
						"text" => "Перейти к обучению",
					),
				),
			),
			array(
				"image" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/d2be32ea320d0c0c82b59997e83c967f.jpg",
				"title" => "Классический кекс с ягодами",
				"start" => "11 апреля",
				"info" => "На курсе вы в полном объеме погрузитесь в профессию кондитер. Научимся готовить конкурентные тортики, булочки и т.д.",
				"id" => 152,
				"status" => array(
					"text" => "Рассрочка",
					"type" => "warning",  // success, error, warning
				),
				"btns" => array(
					array(
						"url" => "/",
						"yellow" => true,
						"text" => "Оплатить рассрочку",
					),
					array(
						"course_id" => 152,
						"text" => "Перейти к обучению",
					),
				),
			),
			array(
				"image" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg",
				"title" => "Печенье «Творожные ушки»",
				"start" => "11 апреля",
				"info" => "На курсе вы в полном объеме погрузитесь в профессию кондитер. Научимся готовить конкурентные тортики, булочки и т.д.",
				"id" => 52,
				"error" => "Ошибка оплаты",
				"status" => array(
					"text" => "Оплачен",
					"type" => "success",  // success, error, warning
				),
				"btns" => array(
					array(
						"course_id" => 52,
						"text" => "Перейти к обучению",
					),
				),
			),
			array(
				"image" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/da80973ca531d248cbcd21b8d86567ae.jpg",
				"title" => "Ванильные маршмеллоу с мармеладом: лапки, пончики, эскимо",
				"start" => "11 апреля",
				"info" => "На курсе вы в полном объеме погрузитесь в профессию кондитер. Научимся готовить конкурентные тортики, булочки и т.д.",
				"id" => 87,
				"status" => array(
					"text" => "Оплачен",
					"type" => "success",  // success, error, warning
				),
				"btns" => array(
					array(
						"course_id" => 87,
						"text" => "Перейти к обучению",
					),
				),
			),
		);
		break;

	case 'get_course_info':

		$data['home'] = 8416;	// id урока который нужно загрузить вместо главной

		$data['content'] = array(

			array(
				"type" => "main",

				"data" => array(
					"title" => "Курс",
					"name" => "Муссовые торты",
					"image" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg",
					"start" => "13 апреля",
					"status" => "error", // success, error, warning
					"status_text" => "Не оплачено",
					"payment_info" => array(
						//"text" => "Следующий платёж 30 апреля 2020, с карты спишется <b>15 580 ₽</b>"
						"show_installment_btn" => true, // кнопка Оплатить рассрочку
						"show_payment_btn" => false // кнопка Оплатить обучение
					),
					"error" => array(
						"title" => "Мы не смогли произвести оплату.",
						"text" => "Попробуйте изменить способ оплаты. Доступ к курсу будет закрыт через 5 дней.",
					)
				)
			),

			array(
				"type" => "main",

				"data" => array(
					"title" => "Курс",
					"name" => "Муссовые торты",
					"image" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg",
					"start" => "13 апреля",
					"status" => "warning", // success, error, warning
					"status_text" => "Рассрочка",
					"payment_info" => array(
						"text" => "Следующий платёж 30 апреля 2020, с карты спишется <b>15 580 ₽</b>",
						"show_installment_btn" => false, // кнопка Оплатить рассрочку
						"show_payment_btn" => true // кнопка Оплатить обучение
					),

				)
			),

			array(
				"type" => "main",

				"data" => array(
					"title" => "Курс",
					"name" => "Муссовые торты",
					"image" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg",
					"start" => "13 апреля",
					"payment_info" => array(
						"text" => "Следующий платёж 30 апреля 2020, с карты спишется <b>15 580 ₽</b>",
						"show_installment_btn" => true, // кнопка Оплатить рассрочку
						"show_payment_btn" => false, // кнопка Оплатить обучение
					),
					"price_table" => array(
						array(
							"title" => "Полная оплата",
							"old_price" => "19 900 ₽",
							"price" => "17 900 ₽",
						),
						array(
							"title" => "В рассрочку на 2 месяца",
							"old_price" => "9 950 ₽ / мес",
							"price" => "8 950 ₽ / мес",
						)
					),

					"buttons" => array(
						array(
							"text" => "В корзину",
							"href" => "https://alinamakarova.ru/personal/cart/",
							"color" => "yellow"
						),
						array(
							"text" => "Еще какая то кнопка",
							"href" => "https://alinamakarova.ru/personal/cart/",
						),
					)

				)
			),

			array(
				"type" => "bonus",

				"data" => array(
					"title" => "Бонусы",
					"image" => "https://alinamakarova.ru/bitrix/templates/alina_super_new/img/picture/photo_1.jpg",
					"html" => "
						<ol>
							<li>Закончи обучение, участвуй в розыгрыше миксера Kitchen Air. Только среди учеников курса Кондитер 2.0</li>
							<li>Оставь отзыв на Яндекс.Картах, получи бесплатный урок.</li>
							<li>Подпишись на нас в соц.сетях. Каждые 2 дня мы публикуем полезные идеи, фотографии и другие материалы</li>
						</ol>
					",
				)
			),

			array(
				"type" => "lessons",
				"data" => array(
					"title" => "Приобрести дополнительно",
					"list" => array(
						array(
							"id" => 456435,
							"image" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/d2be32ea320d0c0c82b59997e83c967f.jpg",
							"name" => "Урок по Cake-Pops",
							"props" => array(
								"complexity" => "Сложность <span class=\"c-green\">легкий</span>",
								"duration" => "50 минут",
								"pages" => "5 страниц",
							),
							"price" => 3590,
						),
						array(
							"id" => 423,
							"image" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/db37ccd8615a28dfeab5834f042a7264.jpg",
							"name" => "Урок по Cake-Pops",
							"props" => array(
								"complexity" => "Сложность <span class=\"c-yellow\">средний</span>",
								"duration" => "60 минут",
								"pages" => "20 страниц",
							),
							"price" => 3590,
						),
						array(
							"id" => 4526,
							"image" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg",
							"name" => "Урок по Cake-Pops",
							"props" => array(
								"complexity" => "Сложность <span class=\"c-red\">тяжелый</span>",
								"duration" => "1 час 20 минут",
								"pages" => "38 страниц",
							),
							"price" => 3590,
						),
					)
				)
			),

		);

		$data['menu'] = array(
			array(
				"title" => "Модуль 1. <span class=\"c-yellow\">Кенди-бар</span>",
				"text" => "7 уроков, 5 домашних работ, 1 бонус",
				"submenu" => array(
					array(
						"id" => 1646,
						"secret" => "7b52edd80b5c283ec80e0481fb16a20c",
						"position" => 1,
						"title" => "Знакомство",
						"text" => "20 февраль 2019",
						"done" => true,
					),
					array(
						"id" => 846,
						"secret" => "7b52edd80b5c283ec80e0481fb16a20c",
						"position" => 2,
						"title" => "Бизнес модуль",
						"text" => "Март 2019",
						"active" => true,
					),
					array(
						"id" => 47,
						"secret" => "7b52edd80b5c283ec80e0481fb16a20c",
						"position" => 3,
						"title" => "Торт наполеон",
						"text" => "Апрель 2019",
					),
					array(
						"id" => 12,
						"secret" => "7b52edd80b5c283ec80e0481fb16a20c",
						"position" => 4,
						"title" => "Макаронс",
						"text" => "2 июня 2019",
					),
					array(
						"id" => 773,
						"secret" => "7b52edd80b5c283ec80e0481fb16a20c",
						"position" => 5,
						"title" => "Выпечка",
						"text" => "29 июня 2019",
					),
					array(
						"id" => 432,
						"secret" => "7b52edd80b5c283ec80e0481fb16a20c",
						"position" => 6,
						"title" => "Упаковка и соц.сети",
						"text" => "1 июля 2019",
					),
					array(
						"id" => 523,
						"secret" => "7b52edd80b5c283ec80e0481fb16a20c",
						"position" => 7,
						"title" => "Дипломная работа",
						"text" => "23 июля 2019",
					),
					array(
						"id" => 742,
						"position" => "gift",
						"secret" => "7b52edd80b5c283ec80e0481fb16a20c",
						"disabled" => true,
						"title" => "Бонус",
						"text" => "Для его получения, выполни все домашние задания",
					),
				)
			),

			array(
				"title" => "Модуль 2. <span class=\"c-yellow\">Торты</span>",
				"text" => "10 уроков, 7 домашних работ, 1 бонус",
				"submenu" => array(
					array(
						"id" => 14646,
						"secret" => "7b52edd80b5c283ec80e0481fb16a20c",
						"position" => 1,
						"title" => "Знакомство",
						"text" => "20 февраль 2019",
					),
					array(
						"id" => 8416,
						"secret" => "7b52edd80b5c283ec80e0481fb16a20c",
						"position" => 2,
						"title" => "Бизнес модуль",
						"text" => "Март 2019",
					),
					array(
						"id" => 447,
						"secret" => "7b52edd80b5c283ec80e0481fb16a20c",
						"position" => 3,
						"title" => "Торт наполеон",
						"text" => "Апрель 2019",
					),
					array(
						"id" => 512,
						"secret" => "7b52edd80b5c283ec80e0481fb16a20c",
						"position" => 4,
						"title" => "Макаронс",
						"text" => "2 июня 2019",
					),
					array(
						"id" => 7733,
						"secret" => "7b52edd80b5c283ec80e0481fb16a20c",
						"position" => 5,
						"title" => "Выпечка",
						"text" => "29 июня 2019",
					),
					array(
						"id" => 4532,
						"secret" => "7b52edd80b5c283ec80e0481fb16a20c",
						"position" => 6,
						"title" => "Упаковка и соц.сети",
						"text" => "1 июля 2019",
					),
					array(
						"id" => 5263,
						"secret" => "7b52edd80b5c283ec80e0481fb16a20c",
						"position" => 7,
						"title" => "Дипломная работа",
						"text" => "23 июля 2019",
					),
					array(
						"id" => 7432,
						"secret" => "7b52edd80b5c283ec80e0481fb16a20c",
						"position" => "gift",
						"disabled" => true,
						"title" => "Бонус",
						"text" => "Для его получения, выполни все домашние задания",
					),
				),
			),

			array(
				"title" => "Дополнительные уроки",
				"text" => "10 уроков.",
				"open" => true,
				"submenu" => array(
					array(
						"id" => 143646,
						"title" => "Пряники: Новогодние игрушки",
					),
					array(
						"id" => 84416,
						"title" => "Куличи",
					),
					array(
						"id" => 4547,
						"title" => "Чизкейк «Малина-мята»",
					),
					array(
						"id" => 5312,
						"title" => "Торт «Джули»",
					),
					array(
						"id" => 77533,
						"title" => "Выпечка",
					),
					array(
						"id" => 43532,
						"title" => "Выпечка",
					),
					array(
						"id" => 55263,
						"title" => "Упаковка и соц.сети",
					),
					array(
						"id" => 75432,
						"title" => "Дипломная работа",
					),
				),
			),

			array(
				"id" => 5443,
				"title" => "Дополнительные уроки",
			),
		);
		break;

	case 'get_course_item':

		$data['info'] = array(
			"id" => 1646,
			"position" => 1,
			"title" => "Знакомство",
			"text" => "20 февраль 2019",
			"done" => true,
		);

		$data['content'] = array(

			array(
				"type" => "main",

				"data" => array(
					"title" => "Онлайн урок",
					"name" => "Пряники: Новогодние игрушки",
					"image" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg",
					//"start" => "13 апреля",

					"price_table" => array(
						array(
							//"title" => "Полная оплата",
							"old_price" => "19 900 ₽",
							"price" => "17 900 ₽",
						),
					),

					"buttons" => array(
						array(
							"text" => "В корзину",
							"href" => "https://alinamakarova.ru/personal/cart/",
							"color" => "yellow"
						),
						array(
							"text" => "Еще какая то кнопка",
							"href" => "https://alinamakarova.ru/personal/cart/",
						),
					)

				)
			),

			array(
				"type" => "html",
				"data" => array(
					"title" => "Урок 1",
					"preview" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/d2be32ea320d0c0c82b59997e83c967f.jpg",
					"html" => "
						<p>Добро пожаловать на 1 занятие. Сегодня мы познакомимся с оборудованием и инвентарём, который понадобиться на протяжении всего курса и поможет достичь наилучшего результата.</p>
						<ul>
							<li>Закончи обучение, участвуй в розыгрыше миксера Kitchen Air. Только среди учеников курса Кондитер 2.0</li>
							<li>Оставь отзыв на Яндекс.Картах, получи бесплатный урок.</li>
							<li>Подпишись на нас в соц.сетях. Каждые 2 дня мы публикуем полезные идеи, фотографии и другие материалы</li>
						</ul>
					",
				)
			),

			array(
				"type" => "files",
				"data" => array(
					"title" => "Материалы",
					"files" => array(
						array(
							"type" => "PDF",
							"title" => "Презентация урока",
							"weight" => "600 КБ",
							"url" => "http://....",
							"color" => "white"
						),
						array(
							"type" => "PDF",
							"title" => "Презентация урока",
							"weight" => "600 КБ",
							"url" => "http://....",
							"color" => "white"
						),
						array(
							"type" => "PDF",
							"title" => "Презентация урока",
							"weight" => "600 КБ",
							"url" => "http://....",
							"color" => "white"
						),
						array(
							"type" => " ",
							"title" => "Скачать всё архивом",
							"weight" => "ZIP",
							"url" => "http://....",
							"color" => "dark"
						),
					)
				)
			),

array(
	"type" => "gallery",
	"data" => array(
		"title" => "Галерея",
		"images" => array(
			array(
				"thumb" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg",
				"src" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg"
			),
			array(
				"thumb" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg",
				"src" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg"
			),
			array(
				"thumb" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg",
				"src" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg"
			),
			array(
				"thumb" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg",
				"src" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg"
			),
			array(
				"thumb" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg",
				"src" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg"
			),
			array(
				"thumb" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg",
				"src" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg"
			),
			array(
				"thumb" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg",
				"src" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg"
			),
			array(
				"thumb" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg",
				"src" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg"
			),
			array(
				"thumb" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg",
				"src" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg"
			),
		)
	)
),

			array(
				"type" => "video",
				"data" => array(
					"videos" => array(
						array(
							"youtubeId" => 'QLDhsOczjsQ'
						),
					)
				)
			),

			array(
				"type" => "video",
				"data" => array(
					"title" => "Бесплатный урок",
					"html" => "
						<ul>
							<li>Отработать теоретическую часть урока</li>
							<li>Подготовить необходимый инвентарь</li>
							<li>Прислать фотографии инвентаря куратору в <a href='#'>диалог</a></li>
						</ul>
						<p><b class=\"c-yellow\">Важно!</b> Чтобы получить бонус, необходимо сдать все домашние задания до конца курса! Последний день сдачи 1 августа 2019</p>
						<br>
					",
					"videos" => array(
						array(
							"name" => "Приветствие",
							"vimeoId" => 169339024
						),
						array(
							"name" => "Подготовка инвентаря и оборудования",
							"vimeoId" => 53356920
						)
					)
				)
			),

			array(
				"type" => "lessons",
				"data" => array(
					"title" => "Приобрести дополнительно",
					"list" => array(
						array(
							"id" => 456435,
							"image" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/d2be32ea320d0c0c82b59997e83c967f.jpg",
							"name" => "Урок по Cake-Pops",
							"props" => array(
								"complexity" => "Сложность <span class=\"c-green\">легкий</span>",
								"duration" => "50 минут",
								"pages" => "5 страниц",
							),
							"price" => 3590,
						),
						array(
							"id" => 423,
							"image" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/db37ccd8615a28dfeab5834f042a7264.jpg",
							"name" => "Урок по Cake-Pops",
							"props" => array(
								"complexity" => "Сложность <span class=\"c-yellow\">средний</span>",
								"duration" => "60 минут",
								"pages" => "20 страниц",
							),
							"price" => 3590,
						),
						array(
							"id" => 4526,
							"image" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg",
							"name" => "Урок по Cake-Pops",
							"props" => array(
								"complexity" => "Сложность <span class=\"c-red\">тяжелый</span>",
								"duration" => "1 час 20 минут",
								"pages" => "38 страниц",
							),
							"price" => 3590,
						),
					)
				)
			),

			array(
				"type" => "html",
				"data" => array(
					"title" => "Вебинар вопрос-ответ",
					"html" => "
						<p><b>Дата:</b> 11 апреля 2019</p>
						<p><b>Продолжительность:</b> 2-3 часа</p>
					"
				)
			),

			array(
				"type" => "html",
				"data" => array(
					"title" => "Домашнее задание",
					"html" => "
						<ul>
							<li>Отработать теоретическую часть урока</li>
							<li>Подготовить необходимый инвентарь</li>
							<li>Прислать фотографии инвентаря куратору в <a href='#'>диалог</a></li>
						</ul>
						<p><b class=\"c-yellow\">Важно!</b> Чтобы получить бонус, необходимо сдать все домашние задания до конца курса! Последний день сдачи 1 августа 2019</p>
					"
				)
			),

			array(
				"type" => "files",
				"hold" => true,
				"data" => array(
					"title" => "Материалы",
					"files" => array(
						array(
							"type" => "PDF",
							"title" => "Презентация урока",
							"weight" => "600 КБ",
							"url" => "#",
							"color" => "white"
						),
						array(
							"type" => "PDF",
							"title" => "Презентация урока",
							"weight" => "600 КБ",
							"url" => "#",
							"color" => "white"
						),
						array(
							"type" => "PDF",
							"title" => "Презентация урока",
							"weight" => "600 КБ",
							"url" => "#",
							"color" => "white"
						),
						array(
							"type" => " ",
							"title" => "Скачать всё архивом",
							"weight" => "ZIP",
							"url" => "#",
							"color" => "dark"
						),
					)
				)
			),

			array(
				"type" => "video",
				"hold" => true,
				"data" => array(
					"title" => "Бесплатный урок",
					"videos" => array(
						array(
							"name" => "Приветствие",
							"poster" => "https://alinamakarova.ru/abeautifulsite/dir_440_300/2b0e41ced814043609f7f5d3403467fe.jpg",
						),
						array(
							"name" => "Подготовка инвентаря и оборудования",
							"poster" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/d2be32ea320d0c0c82b59997e83c967f.jpg"
						)
					)
				)
			),




			array(
				"type" => "html",
				"data" => array(
					"title" => "Как начать обучение?",
					"collapse" => true,
					"html" => "
						<p>Для начала обучения, вам необходимо предоставить паспорт и подписанный договор.</p>
						<ul>
							<li><a href='#step_one'>Шаг 1.</a> Скачайте, ознакомьтесь и подпишите документы. <a href='#step_one'>Перейти к шагу 1</a></li>
							<li><a href='#step_two'>Шаг 2.</a> Загрузите фотографии. Допускаются фотографии сделанные на камеру смартфона или фотоаппарата. <a href='#step_two'>Перейти к шагу 2</a></li>
						</ul>
					"
				)
			),

			array(
				"type" => "html",
				"data" => array(
					"title" => "Как хранятся мои персональные данные?",
					"collapse" => true,
					"html" => "
						<p>Все загруженные документы доступны только <b>модераторам школы «Cake School»</b> и не передаются третьим лицам. ИП Макарова Алина Анатольевна (владелец \"Cake School\") является оператором персональных данных и соответствет всем требованиям 152-ФЗ «О персональных данных»</p>
					"
				)
			),

			array(
				"type" => "video",
				"data" => array(
					"title" => "Видео подсказка",
					"collapse" => true,
					"videos" => array(
						array(
							"name" => "Приветствие",
							"vimeoId" => 169339024
						),
					)
				)
			),

			array(
				"type" => "files",
				"data" => array(
					"anchor" => 'step_one',
					"title" => '<span class="c-yellow"> Шаг 1.</span> Ознакомьтесь с документами',
					"files" => array(
						array(
							"type" => "PDF",
							"title" => "Оферта",
							"weight" => "600 КБ",
							"url" => "#",
							"color" => "white"
						),
						array(
							"type" => "PDF",
							"title" => "Договор",
							"weight" => "600 КБ",
							"url" => "http://....",
							"color" => "white"
						),
						array(
							"type" => " ",
							"title" => "Скачать всё архивом",
							"weight" => "ZIP",
							"url" => "http://....",
							"color" => "dark"
						),
					)
				)
			),

			array(
				"type" => "offer",
				"data" => array(
					"anchor" => 'step_two',
					"title" => '<span class="c-yellow"> Шаг 2.</span> Прикрепите фотографии',
					"confirm_text" => 'Я согласен (-а) на обработку персональных данных, принимаю <a href="#" target="_blank">условия оферты</a>',
					"confirmed" => false,
					"status" => "need_confirm",
							// need_confirm - документы не отправлены
							// in_moderate - документы на модарации
							// need_correct - требуется исправить документы
							// ok - все в порядке
					"files" => array(
						array(
							"title" => "Разворот паспорта с фотографией",
							"id" => 4564,
							"thumb" => "https://static.ca-news.org/upload/ennews/7/630677.1576037942.b.jpg",
							"status" => "success" // success, error
						),
						array(
							"title" => "Разворот паспорта с пропиской",
							"id" => 797,
							"thumb" => "https://static.ca-news.org/upload/ennews/7/630677.1576037942.b.jpg",
							"status" => "error", // success, error
							"message" => "Нечеткая картинка"
						),
						array(
							"title" => "Подписанный договор",
						),
					)
				)
			),
		);

		break;

	case 'get_dialog_menu':

		$data['menu'] = array(
			array(
				"title" => "",
				"items" => array(
					array(
						"id" => 32,
						"image" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/b2b7cc6b05d1a264645514ab28a5440b.jpg",
						"title" => "Евгения Поваренкина",
						"text" => "Евгения: Переписка, тут отражается последн ее присланное сообщение",
						"date" => "22:38",
						"count" => 115,
						"label" => true,
					),
					array(
						"id" => 56,
						"image" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/731c36fc4035cc0ed5ba734df7fee09a.jpg",
						"title" => "Евгения Поваренкина",
						"text" => "Евгения: Переписка, тут отражается последн ее присланное сообщение",
						"date" => "22:38",
						"unread" => true,
					),
					array(
						"id" => 76,
						"image" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/5493f607fb95e2a37109a0736e628953.jpg",
						"title" => "Евгения Поваренкина",
						"text" => "Евгения: Переписка, тут отражается последн ее присланное сообщение",
						"date" => "22:38",
						"count" => 14,
					),
					array(
						"id" => 34,
						"image" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/e7bb7610b93e399e84cf9c28a0183434.jpg",
						"title" => "Евгения Поваренкина",
						"text" => "Евгения: Переписка, тут отражается последн ее присланное сообщение",
						"date" => "22:38",
						"count" => 0,
					),
					array(
						"id" => 52,
						"image" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/b2b7cc6b05d1a264645514ab28a5440b.jpg",
						"title" => "Евгения Поваренкина",
						"text" => "Евгения: Переписка, тут отражается последн ее присланное сообщение",
						"date" => "22:38",
						"count" => 115,
					),
					array(
						"id" => 758,
						"image" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/731c36fc4035cc0ed5ba734df7fee09a.jpg",
						"title" => "Евгения Поваренкина",
						"text" => "Евгения: Переписка, тут отражается последн ее присланное сообщение",
						"date" => "22:38",
						"count" => 0,
					),
					array(
						"id" => 123,
						"image" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/5493f607fb95e2a37109a0736e628953.jpg",
						"title" => "Евгения Поваренкина",
						"text" => "Евгения: Переписка, тут отражается последн ее присланное сообщение",
						"date" => "22:38",
						"count" => 5,
					),
					array(
						"id" => 98,
						"image" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/e7bb7610b93e399e84cf9c28a0183434.jpg",
						"title" => "Евгения Поваренкина",
						"text" => "Евгения: Переписка, тут отражается последн ее присланное сообщение",
						"date" => "22:38",
						"count" => 0,
					),

				)
			),

		);
		break;

	case 'search_dialog_menu':

		if ( strlen($_POST['query']) > 10 ){
			$data['menu'] = array(

			);
		} else {
			$data['menu'] = array(
				array(
					"title" => "Студенты",
					"items" => array(
						array(
							"id" => 32,
							"image" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/b2b7cc6b05d1a264645514ab28a5440b.jpg",
							"title" => "Евгения Поваренкина",
							"text" => "Евгения: Переписка, тут отражается последн ее присланное сообщение",
							"date" => "22:38",
							"count" => 115,
						),
						array(
							"id" => 56,
							"image" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/731c36fc4035cc0ed5ba734df7fee09a.jpg",
							"title" => "Евгения Поваренкина",
							"text" => "Евгения: Переписка, тут отражается последн ее присланное сообщение",
							"date" => "22:38",
							"count" => 0,
						),
						array(
							"id" => 76,
							"image" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/5493f607fb95e2a37109a0736e628953.jpg",
							"title" => "Евгения Поваренкина",
							"text" => "Евгения: Переписка, тут отражается последн ее присланное сообщение",
							"date" => "22:38",
							"count" => 14,
						),
					)
				),
				array(
					"title" => "Сообщение",
					"items" => array(
						array(
							"id" => 34,
							"image" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/e7bb7610b93e399e84cf9c28a0183434.jpg",
							"title" => "Евгения Поваренкина",
							"text" => "Евгения: Переписка, тут отражается последн ее присланное сообщение",
							"date" => "22:38",
							"count" => 0,
						),
						array(
							"id" => 52,
							"image" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/b2b7cc6b05d1a264645514ab28a5440b.jpg",
							"title" => "Евгения Поваренкина",
							"text" => "Евгения: Переписка, тут отражается последн ее присланное сообщение",
							"date" => "22:38",
							"count" => 115,
						),
						array(
							"id" => 758,
							"image" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/731c36fc4035cc0ed5ba734df7fee09a.jpg",
							"title" => "Евгения Поваренкина",
							"text" => "Евгения: Переписка, тут отражается последн ее присланное сообщение",
							"date" => "22:38",
							"count" => 0,
						),
						array(
							"id" => 123,
							"image" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/5493f607fb95e2a37109a0736e628953.jpg",
							"title" => "Евгения Поваренкина",
							"text" => "Евгения: Переписка, тут отражается последн ее присланное сообщение",
							"date" => "22:38",
							"count" => 5,
						),
						array(
							"id" => 98,
							"image" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/e7bb7610b93e399e84cf9c28a0183434.jpg",
							"title" => "Евгения Поваренкина",
							"text" => "Евгения: Переписка, тут отражается последн ее присланное сообщение",
							"date" => "22:38",
							"count" => 0,
						),

					)
				),

			);
		}

		break;

	case 'get_dialog_more':
	case 'get_dialog_item':


		if ( !$is_curator && rand(0, 3) === 1 ){

			/// если ученик пришел впервые и еще ничего не писал ,показываем приветственную надпись

			$data['data'] = array(
				"welcome" => array(
					"title" => "Добро пожаловать в диалог «Мегакурс 1»",
					"text" => "Как только начнется обучение, вы сможете задать возникающие вопросы, сдать домашнюю работу, сообщить о проблемах",
					"image" => "https://alinamakarova.ru/bitrix/templates/alina_super_new/img/picture/photo_1.jpg",
					"curator" => "Евгения Тортикова",
					"online" => true
				),
			);
		} else {

			$data['data'] = array(

				"main" => array(
					"image" => "https://alinamakarova.ru/bitrix/templates/alina_super_new/img/picture/photo_1.jpg",
					"title" => "Евгения Удельная"
				),

				"chat" => array(
					array(
						"id" => rand(1000000, 9999999),
						"date" => 1586704034506,
						"author" => array(
							"id" => 100,
							"image" => "https://alinamakarova.ru/bitrix/templates/alina_super_new/img/picture/photo_1.jpg",
							"name" => "Евгения Удельная",
							"online" => true
						),
						"message" => "Минобороны РФ выложило в соцсетях скриншот из игры под видом «неоспоримого подтверждения обеспечения Соединенными Штатами прикрытия боеспособных отрядов ИГИЛ». Пользователи соцсетей решили поддержать ведомство и устроили флешмоб «неоспоримых",
						"reply" => array(
							"id" => 233,
							"author" => "Евгения Тортикова",
							"date" => 1586704032506,
							"text" => "Минобороны РФ выложило в  под видом «неоспорим выложило в соцсетях»",
						),
						"assets" => array(
							array(
								"id" => 431,
								"type" => "image",
								"name" => "JPG",
								"url" => "http://...",
								"weight" => "87 КБ",
								"thumb" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/419ad784c7c8dbef00988f7cf964e8b1.jpg",
							),
							array(
								"id" => 564,
								"type" => "document",
								"url" => "http://...",
								"name" => "Оферта.docx",
								"weight" => "87 КБ",
							),
							array(
								"id" => 653,
								"type" => "archive",
								"url" => "http://...",
								"name" => "Оферта.zip",
								"weight" => "87 КБ",
							),
							array(
								"id" => 876,
								"type" => "image",
								"url" => "http://...",
								"name" => "JPG",
								"weight" => "87 КБ",
								"thumb" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/419ad784c7c8dbef00988f7cf964e8b1.jpg",
							),
							array(
								"id" => 979,
								"type" => "video",
								"name" => "Оферта.mp4",
								"url" => "http://...",
								"weight" => "17 MБ",
							),
							array(
								"id" => 908,
								"type" => "image",
								"name" => "JPG",
								"url" => "http://...",
								"weight" => "87 КБ",
								"thumb" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/419ad784c7c8dbef00988f7cf964e8b1.jpg",
							),
						)
					),

					array(
						"id" => rand(1000000, 9999999),
						"date" => 1586704034506,
						"author" => array(
							"id" => 100,
							"image" => "https://alinamakarova.ru/bitrix/templates/alina_super_new/img/picture/photo_1.jpg",
							"name" => "Евгения Удельная",
							"online" => true
						),
						"message" => "Минобороны РФ выложило в соцсетях скриншот из игры под видом «неоспоримого ",
					),
					array(
						"id" => rand(1000000, 9999999),
						"date" => 1586704034506,
						"author" => array(
							"id" => 555,
							"image" => "https://alinamakarova.ru/bitrix/templates/alina_super_new/img/picture/photo_1.jpg",
							"name" => "Евгения Удельная",
							"online" => true
						),
						"message" => "Минобороны РФ выложило в соцсетях скриншот из игры под видом «неоспоримого ",
					),

					array(
						"id" => rand(1000000, 9999999),
						"date" => 1586704034506,
						"author" => array(
							"id" => 100,
							"image" => "https://alinamakarova.ru/bitrix/templates/alina_super_new/img/picture/photo_1.jpg",
							"name" => "Евгения Удельная",
							"online" => true
						),
						"message" => "Минобороны РФ выложило в соцсетях скриншот из игры под видом «неоспоримого подтверждения обеспечения Соединенными Штатами прикрытия боеспособных отрядов ИГИЛ». Пользователи соцсетей решили поддержать ведомство и устроили флешмоб «неоспоримых",
						"reply" => array(
							"id" => 233,
							"author" => "Евгения Тортикова",
							"date" => 1586704032506,
							"text" => "Минобороны РФ выложило в соцсетях скриншот из игры под видом «неоспорим выложило в соцсетях»",
						),
					),
					array(
						"id" => rand(1000000, 9999999),
						"date" => 1586704034506,
						"author" => array(
							"id" => 555,
							"image" => "https://alinamakarova.ru/bitrix/templates/alina_super_new/img/picture/photo_1.jpg",
							"name" => "Евгения Удельная",
							"online" => true
						),
						"message" => "Минобороны РФ выложило в соцсетях скриншот из игры под видом «неоспоримого подтверждения обеспечения Соединенными Штатами прикрытия боеспособных отрядов ИГИЛ». Пользователи соцсетей решили поддержать ведомство и устроили флешмоб «неоспоримых",
						"reply" => array(
							"id" => 233,
							"author" => "Евгения Тортикова",
							"date" => 1586704032506,
							"text" => "Минобороны РФ выложило в соцсетях скриншот из игры под видомжило в соцсетях скриншот из игры под видомжило в соцсетях скриншот из игры под видом «неоспорим выложило в соцсетях»",
						),
					),

					array(
						"id" => rand(1000000, 9999999),
						"date" => 1586704034506,
						"author" => array(
							"id" => 555,
							"image" => "https://alinamakarova.ru/bitrix/templates/alina_super_new/img/picture/photo_1.jpg",
							"name" => "Евгения Удельная",
							"online" => true
						),
						"removed" => true
					),

					array(
						"id" => rand(1000000, 9999999),
						"date" => 1586704034506,
						"author" => array(
							"id" => 100,
							"image" => "https://alinamakarova.ru/bitrix/templates/alina_super_new/img/picture/photo_1.jpg",
							"name" => "Евгения Удельная",
							"online" => true
						),
						"assets" => array(
							array(
								"id" => 431,
								"type" => "image",
								"name" => "JPG",
								"url" => "http://...",
								"weight" => "87 КБ",
								"thumb" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/419ad784c7c8dbef00988f7cf964e8b1.jpg",
							),
							array(
								"id" => 564,
								"type" => "document",
								"name" => "Оферта.docx",
								"url" => "http://...",
								"weight" => "87 КБ",
							),
							array(
								"id" => 653,
								"type" => "archive",
								"name" => "Оферта.zip",
								"url" => "http://...",
								"weight" => "87 КБ",
							),
							array(
								"id" => 876,
								"type" => "image",
								"name" => "JPG",
								"url" => "http://...",
								"weight" => "87 КБ",
								"thumb" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/419ad784c7c8dbef00988f7cf964e8b1.jpg",
							),
							array(
								"id" => 979,
								"type" => "video",
								"url" => "http://...",
								"name" => "Оферта.mp4",
								"weight" => "17 MБ",
							),
							array(
								"id" => 908,
								"type" => "image",
								"name" => "JPG",
								"url" => "http://...",
								"weight" => "87 КБ",
								"thumb" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/419ad784c7c8dbef00988f7cf964e8b1.jpg",
							),
						)
					),

					array(
						"id" => rand(1000000, 9999999),
						"date" => 1586704034506,
						"author" => array(
							"id" => 555,
							"image" => "https://alinamakarova.ru/bitrix/templates/alina_super_new/img/picture/photo_1.jpg",
							"name" => "Евгения Удельная",
							"online" => true
						),
						"assets" => array(
							array(
								"id" => 431,
								"type" => "image",
								"url" => "http://...",
								"name" => "JPG",
								"weight" => "87 КБ",
								"thumb" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/419ad784c7c8dbef00988f7cf964e8b1.jpg",
							),
							array(
								"id" => 564,
								"type" => "document",
								"url" => "http://...",
								"name" => "Оферта.docx",
								"weight" => "87 КБ",
							),
							array(
								"id" => 653,
								"type" => "archive",
								"name" => "Оферта.zip",
								"url" => "http://...",
								"weight" => "87 КБ",
							),
							array(
								"id" => 876,
								"type" => "image",
								"url" => "http://...",
								"name" => "JPG",
								"weight" => "87 КБ",
								"thumb" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/419ad784c7c8dbef00988f7cf964e8b1.jpg",
							),
							array(
								"id" => 979,
								"type" => "video",
								"url" => "http://...",
								"name" => "Оферта.mp4",
								"weight" => "17 MБ",
							),
							array(
								"id" => 908,
								"type" => "image",
								"name" => "JPG",
								"url" => "http://...",
								"weight" => "87 КБ",
								"thumb" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/419ad784c7c8dbef00988f7cf964e8b1.jpg",
							),
						)
					),

					array(
						"id" => rand(1000000, 9999999),
						"date" => 1586704034506,
						"author" => array(
							"id" => 789,
							"image" => "https://alinamakarova.ru/bitrix/templates/alina_super_new/img/picture/photo_1.jpg",
							"name" => "Евгения Удельная",
							"online" => true
						),
						"removed" => true
					),

					array(
						"id" => rand(1000000, 9999999),
						"date" => 1586704034506,
						"author" => array(
							"id" => 789,
							"image" => "https://alinamakarova.ru/bitrix/templates/alina_super_new/img/picture/photo_1.jpg",
							"name" => "Евгения Удельная",
							"online" => true
						),
						"assets" => array(
							array(
								"id" => 908,
								"type" => "image",
								"url" => "http://...",
								"name" => "JPG",
								"weight" => "87 КБ",
								"thumb" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/419ad784c7c8dbef00988f7cf964e8b1.jpg",
							),
						)
					),

					array(
						"id" => rand(1000000, 9999999),
						"date" => 1586704034506,
						"author" => array(
							"id" => 555,
							"image" => "https://alinamakarova.ru/bitrix/templates/alina_super_new/img/picture/photo_1.jpg",
							"name" => "Евгения Удельная",
							"online" => true
						),
						"assets" => array(
							array(
								"id" => 908,
								"url" => "http://...",
								"type" => "image",
								"name" => "JPG",
								"weight" => "87 КБ",
								"thumb" => "https://alinamakarova.ru/abeautifulsite/dir_544_380/419ad784c7c8dbef00988f7cf964e8b1.jpg",
							),
						)
					),
					array(
						"id" => rand(1000000, 9999999),
						"date" => 1586704034506,
						"author" => array(
							"id" => 100,
							"image" => "https://alinamakarova.ru/bitrix/templates/alina_super_new/img/picture/photo_1.jpg",
							"name" => "Евгения Удельная",
							"online" => true
						),
						"message" => "Минобороны РФ выложило в соцсетях скриншот из игры под видом «неоспоримого подтверждения обеспечения Соединенными Штатами прикрытия боеспособных отрядов ИГИЛ». Пользователи соцсетей решили поддержать ведомство и устроили флешмоб «неоспоримых",
						"reply" => array(
							"id" => 233,
							"author" => "Евгения Тортикова",
							"date" => 1586704032506,
							"text" => "Минобороны РФ выложило в соцсетях скриншот из игры под видом ороны РФ выложило в соцсетях скриншот из игры под видом ороны РФ выложило в соцсетях скриншот из игры под видом «неоспорим выложило в соцсетях»",
						),
						"assets" => array(
							array(
								"id" => 564,
								"type" => "document",
								"url" => "http://...",
								"name" => "Оферта.docx",
								"weight" => "87 КБ",
							),
							array(
								"id" => 653,
								"type" => "archive",
								"name" => "Оферта.zip",
								"url" => "http://...",
								"weight" => "87 КБ",
							),
							array(
								"id" => 979,
								"type" => "video",
								"name" => "Оферта.mp4",
								"url" => "http://...",
								"weight" => "17 MБ",
							),
						)
					),
					array(
						"id" => rand(1000000, 9999999),
						"date" => 1586704034506,
						"author" => array(
							"id" => 555,
							"image" => "https://alinamakarova.ru/bitrix/templates/alina_super_new/img/picture/photo_1.jpg",
							"name" => "Евгения Удельная",
							"online" => true
						),
						"message" => "Минобороны РФ выложило в соцсетях скриншот из игры под видом «неоспоримого подтверждения обеспечения Соединенными Штатами прикрытия боеспособных отрядов ИГИЛ». Пользователи соцсетей решили поддержать ведомство и устроили флешмоб «неоспоримых",
						"reply" => array(
							"id" => 233,
							"author" => "Евгения Тортикова",
							"date" => 1586704032506,
							"text" => "Минобороны РФ выложило в соцсетях скриншот из игры под видомРФ выложило в соцсетях скриншот из игры под видомРФ выложило в соцсетях скриншот из игры под видом «неоспорим выложило в соцсетях»",
						),
						"assets" => array(
							array(
								"id" => 564,
								"type" => "document",
								"url" => "http://...",
								"name" => "Оферта.docx",
								"weight" => "87 КБ",
							),
							array(
								"id" => 653,
								"type" => "archive",
								"url" => "http://...",
								"name" => "Оферта.zip",
								"weight" => "87 КБ",
							),
							array(
								"id" => 979,
								"type" => "video",
								"name" => "Оферта.mp4",
								"url" => "http://...",
								"weight" => "17 MБ",
							),
						)
					),

				),

				"done" => $action == 'get_dialog_more', // если еще есть сообщения
			);
		}



		break;

	case 'upload_offer_file':
	case 'upload_dialog_file':

		/**
		 * @type = String (image, document, archive, video)
		 * @name = String - заголовок
		 * @weight = String - Вес файла, текстом в упорщенном виде
		 * @thumb = String - Ссылка на превью если это изображение
		 */

		$file = $_FILES['file'];

		$image_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");

		$data['data'] = array(
			"id" => rand(1000000, 9999999),
			"type" => 'document',
			"name" => $file['name'],
			"weight" => ($file['size'] / 1000).' КБ',
		);

		if (in_array($file['type'], $image_types)){
			$data['data']['thumb'] = "https://alinamakarova.ru/abeautifulsite/dir_544_380/419ad784c7c8dbef00988f7cf964e8b1.jpg";
			$data['data']['type'] = "image";
		}

		break;

	case 'add_dialog_message':

		$data['status'] = true;

		break;

	case 'remove_dialog_post':

		$data['status'] = true;

		break;

	case 'unread_dialog_post':

		$data['status'] = true;

		break;

	case 'send_offer_files':

		$data['status'] = true;

		break;

	case 'restore_dialog_post':

		$data['status'] = true;

		$data['item'] = array(
				"id" => rand(1000000, 9999999),
				"date" => 1586704034506,
				"author" => array(
					"id" => 555,
					"image" => "https://alinamakarova.ru/bitrix/templates/alina_super_new/img/picture/photo_1.jpg",
					"name" => "Евгения Удельная",
					"online" => true
				),
				"message" => "Минобороны РФ выложило в соцсетях скриншот из игры под видом «неоспоримого подтверждения обеспечения Соединенными Штатами прикрытия боеспособных отрядов ИГИЛ». Пользователи соцсетей решили поддержать ведомство и устроили флешмоб «неоспоримых",
				"reply" => array(
					"id" => 233,
					"author" => "Евгения Тортикова",
					"date" => 1586704032506,
					"text" => "Минобороны РФ выложило в соцсетях скриншот из игры под видомжило в соцсетях скриншот из игры под видомжило в соцсетях скриншот из игры под видом «неоспорим выложило в соцсетях»",
				),
			);

		break;
}


echo json_encode($data);
?>
import './css/style.scss'

import Vue from 'vue';

Vue.config.devtools = true;

import {
	router,
	store,
	filters,
	library
} from './middleware'

Object.keys(filters).forEach(name => {
	Vue.filter(name, filters[name]);
});

Object.keys(library).forEach(name => {
	Vue.use(library[name].plugin, library[name].options || {});
});

import * as components from './components';

Object.keys(components).forEach(name => {
	Vue.component(name, components[name]);
});

import index from './index.vue';

if ( document.getElementById('app') ){
	window.app = new Vue({
		el: '#app',
		router,
		store,
		render: h => h(index)
	});
}


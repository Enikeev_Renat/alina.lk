export default [
	{
		group: 'Основные',
		name: 'home',
		icon: 'home',
		title: 'Главная',
		submenu: [],
		count: 0,
		href: 'https://alinamakarova.ru/',
	},
	{
		group: 'Основные',
		name: 'notifications',
		icon: 'bell',
		title: 'Оповещения',
		submenu: [],
		count: 0,
		href: '/notifications',
		disabled: true,
	},
	{
		group: 'Основные',
		name: 'cart',
		icon: 'cart',
		title: 'Корзина',
		submenu: [],
		count: 0,
		href: 'https://alinamakarova.ru/personal/cart/',
		//href: '/cart',
	},
	{
		group: 'Школа',
		name: 'lessons',
		title: 'Онлайн-уроки',
		submenu: [],
		count: 0,
		href: 'https://alinamakarova.ru/mycourses.php',
	//	href: '/lessons',
	},
	{
		group: 'Школа',
		name: 'courses',
		title: 'Онлайн-курсы',
		count: 0,
		href: '/courses',
		submenu: [],
	},
	{
		group: 'Школа',
		name: 'dialogs',
		title: 'Сообщения',
		count: 0,
		href: '/dialogs',
		submenu: [],
	},
	{
		group: 'Поддержка',
		name: 'support',
		icon: 'service',
		title: 'Техподдержка',
		submenu: [],
		count: 0,
		href: '/support',
		disabled: true,
	},
	{
		group: 'Поддержка',
		name: 'faq',
		icon: 'question',
		title: 'FAQ',
		submenu: [],
		count: 0,
		href: '/faq',
		disabled: true,
	},
]